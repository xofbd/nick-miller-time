# It's Nick Miller Time
A simple web app that randomly displays a quote from [Nick Miller](https://en.wikipedia.org/wiki/List_of_New_Girl_characters#Nick_Miller)

## License
The project is distributed under the GNU GPL license. Please see `COPYING` for more information.