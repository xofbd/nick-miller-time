package main

import (
	"context"
	"fmt"
	"github.com/jackc/pgx/v5"
	"net/http"
	"os"
)

var PORT string = os.Getenv("PORT")
var URL_DB string = os.Getenv("URL_DB")

func indexHandler(w http.ResponseWriter, r *http.Request) {
	conn, err := pgx.Connect(context.Background(), URL_DB)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Unable to connect to database: %v\n", err)
	}
	defer conn.Close(context.Background())

	var quote string
	err = conn.QueryRow(context.Background(), "SELECT quote FROM quotes ORDER BY RANDOM() LIMIT 1;").Scan(&quote)
	if err != nil {
		fmt.Fprintf(os.Stderr, "QueryRow failed: %v\n", err)
		os.Exit(1)
	}
	w.Write([]byte(fmt.Sprintf("%s", quote)))
}

func main() {

	mux := http.NewServeMux()

	mux.HandleFunc("/", indexHandler)
	http.ListenAndServe(":"+PORT, mux)
}
